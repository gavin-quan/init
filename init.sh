#!/bin/sh
yum remove -y docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine

yum install -y yum-utils \
           device-mapper-persistent-data \
           lvm2

yum-config-manager \
     --add-repo \
     https://download.docker.com/linux/centos/docker-ce.repo
     
#yum-config-manager \
#     --add-repo \
#     http://mirrors.aliyun.com/repo/Centos-7.repo

yum-config-manager --enable docker-ce-nightly
yum makecache fast

yum install -y docker-ce
systemctl enable docker
systemctl start docker
groupadd docker
usermod -aG docker $USER

#rm -rf /etc/yum.repos.d/xxx.repo
#yum clean all
#yum makecache

yum -y install \
       gcc automake autoconf libtool make crontabs wget git screen cron \
       telnet telnet-server iptables-services ntp sshpass openssl unzip ntpdate erase podman buildah 

rm -rf /etc/localtime
ln /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
ntpdate cn.pool.ntp.org
hwclock --systohc && hwclock -w

service iptables stop
systemctl stop firewalld.service

touch /etc/docker/daemon.json
echo '{"registry-mirrors": ["https://mlvvp7dl.mirror.aliyuncs.com"],"hosts":["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"], "insecure-registries": ["'$(ip addr|grep 192|awk '{print $4}')':5020"]}' >> /etc/docker/daemon.json
sed -i 's!ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock!ExecStart=/usr/bin/dockerd!g' /lib/systemd/system/docker.service
systemctl daemon-reload && systemctl reset-failed docker.service && systemctl restart docker;
docker swarm init

mkdir /iredpay
cd /iredpay
mkdir -p data/{activemq/data,activemq/logs,docker-registry,elasticsearch/data,elasticsearch/logs,elk/data,elk/logs,gitlab/data,gitlab/logs,grafna/data,grafna/logs,jumpserver/keys,jumpserver/data,jumpserver/mysql,jumpserver/redis,kafka/data,kafka/logs,kafka/run,kibana/logs,kylin/data,logstash/logs,minio/config,minio/data,mongo/data,mongo/logs,mysql/binlog,mysql/data,mysql/files,mysql/logs,mysql/sock,nacos/logs,redis/data,rocketmq/rmq/data,rocketmq/rmq/logs,rocketmq/rmqs/data,rocketmq/rmqs/logs,showdoc/data,sonatype/data,xxljob/logs,zentao/data,zk/data,zk/logs,app}
chmod -R a+rw data

git clone https://gitlab.com/uktp/init.git

echo export APP_ROOT=/iredpay >> /etc/profile
echo export DOCKER_HOST_IP=172.17.0.1 >> /etc/profile
echo export HOST_IP=$(ip addr|grep 192|awk '{print $4}') >> /etc/profile
source /etc/profile

docker stack deploy --resolve-image=changed -c $APP_ROOT/init/compose/s1.yml s1
docker stack deploy --resolve-image=changed -c $APP_ROOT/init/compose/s2.yml s2
