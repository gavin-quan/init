# gitlab配置文件参考
# https://docs.gitlab.com/omnibus/settings/gitlab.yml.html
# https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template
# 配置http协议所使用的访问地址,不加端口号默认为80
external_url 'http://gitlab.test.server.com'
# 配置ssh协议所使用的访问地址和端口
gitlab_rails['gitlab_ssh_host'] = 'gitlab.test.server.com'
gitlab_rails['gitlab_shell_ssh_port'] = 4005 # 此端口是run时22端口映射的4005端口